<?php

class emappingArchiveHandlerDefault extends eMappingMapHandlerDefault {
  var $map_entity = FALSE;
  var $uri = ''; // location of downloadable file
                 // @todo: should be derived from map_entity file field object

  public function __construct($options = array()) {
    parent::__construct($options);
  }
  
  function DefineOptions() {
    $options = parent::DefineOptions();
    $options['fileFormat'] = 'zip';
    return $options;
  }
  
  // when we go to D8 this will be relevant
    // public function buildOptionsForm(&$form, FormStateInterface $form_state) {
  // until then, we use the old school method
  public function buildOptionsForm(&$form, $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $hidden = array();
    foreach ($hidden as $hidethis) {
      $form[$hidethis]['#type'] = 'hidden';
    }
    $form['fileFormat'] = array(
      '#title' => t('File Type'),
      '#description' => t('Type of archive to store rendered files in.'),
      '#type' => 'select',
      '#options' => array(
        'zip' => 'Zip',
        'targz' => 'tar.gz'
      ),
      '#required' => TRUE,
      '#default_value' => $this->options['fileFormat'],
    );
  }
  
  function defaultSettings() {
    return array(
      'compression' => FALSE,
      'fileFormat'  => 'tar',
    );
  }

  function getOptions() {
    $options = $this->defaultSettings();
    drupal_set_message("Applied options from source: " . print_r($options,1));
    return $options;
  }
  
  function getData() {
    return array();
  }

  function render($mapinfo = array()) {
    $filenames = array();
    if (!$this->map_entity) {
      return;
    }

    if (!is_object($this->map_entity)) {
      dpm ( "Could not load object $mid - exiting.");
      //echo "getMapArray() = <pre>" . print_r($stuff,1) . "<pre>";
    } else {
      // * Check for map archive existence:
      //   - if archive does not exist, try to create 
      // * Check if we have called a forced refresh:
      //   - if no refresh called just show the existing file
      // this is essentially the prototype for Display plugin code here
      // we get our map, then load layers
      // then add a display plugin for each Layer of the desired type
      $obj = $this->map_entity;
      $stuff =  $obj->getMapArray();
      $outdir = DRUPAL_ROOT . "/sites/default/files/emapping/";
      $archive_name = 'emapping-archive-' . $obj->mid . '.zip';
      $ziplist = array();
      ctools_include('plugins');
      $plugins = ctools_plugin_get_info('emapping', 'Component');
      //dpm($plugins, "info emapping plugins");
      $plugins = ctools_get_plugins('emapping', 'Component');
      //dpm($plugins, "Component emapping plugins");
      $class = FALSE;
      $plugin = ctools_get_plugins('emapping', 'Component', 'EMappingLayerShapeFileHandler');
      //dpm($plugin, "EMappingLayerShapeFileHandler plugin");
      $class = ctools_plugin_get_class($plugin, 'handler');
      dpm($stuff['layers'], "map layers found");
      // iterate through the layers
      if ($class) {
        foreach ($stuff['layers'] as $thislayer) {
          if (isset($thislayer['source']['entity_id'])) {
            // load layer entity and set on layer display plugin
            // instantiate the layer class display EMappingLayerShapeFileHandler
              // on display object, set layer_entity
            // call render method
            $layer = entity_load_single('emapping_layer', $thislayer['entity_id']);
            // set the layers display class
            // @todo: setting the display_class on the layer should occur
            //        in the emapping.module code, as the display_class
            //        should be stored in the emapping_layer table as a column
            $layer->display_class = $class;
            // instantiate the display_class object
            // @todo: this should occur in layer->prepare()
            $layer->layer_display = new $class;
            $layer->layer_display->layer_entity = $layer;
            $layer->layer_display->out_dir = $outdir;
            $layer->layer_display->render();
            foreach ($layer->layer_display->getFiles() as $f) {
              $ziplist[] = $f;
            }
          }
          $cmd = "cd $outdir; ";
          $cmd .= "rm $archive_name; ";
          $cmd .= "zip $archive_name " . implode(' ', $ziplist);
          dpm( $cmd, 'archive command');
          shell_exec($cmd);
        }
        $this->uri = base_path() . "sites/default/files/emapping/$archive_name";
      } else {
        dpm("Could not load ctools plugin EMappingLayerShapeFileHandler");
      }
    }
  }
  
}
?>
