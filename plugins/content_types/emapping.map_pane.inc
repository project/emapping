<?php
/**
 * @file
 * Pane for multiple data layer leaflet maps.
 */

$plugin = array(
  'title' => t('eMapping map'),
  'description' => t('Display a eMapping map implementation.'),
  'category' => array(t('eMapping')),
  'single' => TRUE,
  'defaults' => array(),
  'admin info' => 'emapping_map_pane_admin_info',
  'render callback' => 'emapping_map_pane_render',
  'edit form' => 'emapping_map_pane_edit_form',
  'all contexts' => TRUE, 
);

/**
 * Callback for page_manager admin UI information.
 */
function emapping_map_pane_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';

    $layer_count = count($conf['map_settings']['info']['data']);
    $admin_info = t('@map_base (@count data @layers)', array(
      '@map_base' => $conf['map_settings']['mid'],
      '@count' => $layer_count,
      '@layers' => format_plural($layer_count, 'layer', 'layers'),
    ));
    $block->content = $admin_info;

    return $block;
  }
}

/**
 * Callback to render the pane.
 */
function emapping_map_pane_render($subtype, $conf, $panels_args, $context) {
  // Build the pane.
  $pane = new stdClass();
  $pane->module = 'emapping';
  // This title can/should be overriden in the page manager UI.
  $pane->title = 'eMapping Map';

  $pane->content = emapping_map_pane_render_layered_map($conf, $context);

  return $pane;
}

/**
 * Helper function to generate the map markup based on the pane config.
 */
function emapping_map_pane_render_layered_map($conf, $context) {

  // Gather information about the leaflet base and data layers.
  $mid = $conf['map_settings']['mid'];
  $results = entity_load('emapping_map', array($mid));
  if (!is_object($results[$mid])) {
    return t("There was a problem loading the entity $mid");
  }
  // otherwise, we have a valid map object so let's handle it
  $map = $results[$mid];
  $map_data = $map->getMapArray();


  // Allow other modules to alter the map data.
  // disabled till we figure out if this needs to remain or if it is 
  // to be taken care of by the rendering engines or BOTH
  // drupal_alter('emapping_map_pane', $map_base_info, $feature_layers);

  //return render($map);
  return "<pre>" . print_r($map_data,1) . "</pre>";
}


/**
 * Edit form for the pane's settings.
 */
function emapping_map_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Buildmap selector.
  $base_options = array();
  $result = db_query("select name, description, mid from {emapping_map}");

  while ($record = $result->fetchAssoc()) {
    $base_options[$record['mid']] = t($record['name']);
  }
  if (count($record) == 0) {
    // we have no maps defined, return a message
      // Build the data layers selector.
    $form['map_settings']['info'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => 'eMapping Map Layer Configurations',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => 'No eMapping maps have been defined',
    );
    return $form;
  }
  // otherwise, we have maps available, so go ahead and show a list
  // The default selection is the first one, or the previously selected one.
  $default_map = key($base_options);
  if (isset($conf['map_settings']['mid'])) {
    $default_map = $conf['map_settings']['mid'];
  }
  $form['map_settings']['mid'] = array(
    '#title' => t('eMap Definition'),
    '#type' => 'select',
    '#options' => $base_options,
    '#default_value' => $default_map,
    '#required' => TRUE,
    '#description' => t(
        'Select the eMap.'
    ),
  );
  
  // Provide some UI help for setting up multi-layer maps.
  $data_layers_description = t("MapID $default_map selected.<br>");
  $data_layers_description .= t('Specify layer specific style options.');

  // Build the data layers selector.
  $form['map_settings']['info'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => "eMapping Map Layer Configurations (MapID: $default_map)",
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => $data_layers_description,
  );

  // Grab the available layers.
  $results = entity_load('emapping_map',array($default_map));
  if (!is_object($results[$default_map])) {
    $form['map_settings']['info']['#description'] = t("There was a problem loading the entity $default_map - cannot configure map.");
    return $form;  
  }
  $map = $results[$default_map];
  $map_info = $map->getMapArray();
  // this returns array of form:
  //  'name'
  //  'title'
  //  'entity_id'
  //  'type'
  //  'layers' => array(
  //    name, title, entity_id, type (bundle) 
  //    source = > array(name, title, data_uri)
  //  )
  $layers = $map_info['layers'];
  foreach ($layers as $layer) {
    $layerid = $layer['entity_id'];
    $layer_config = '';
    if (isset($conf['map_settings']['info']['data']['layer_' . $layerid])) {
      $layer_config = $conf['map_settings']['info']['data']['layer_' . $layerid];
    }
    $form['map_settings']['info']['data']['layer_' . $layerid] = array(
      //'#type' => 'text',
      '#type' => 'textfield',
      '#title' => t("$layer[name] Settings "),
      '#default_value' => $layer_config,
      '#description' => t("$layer[name] of type: $layer[type] from Source: " . $layer['source']['data_uri']),
      '#size' => 80,
    );
  }

  return $form;
}

/**
 * Submit handler just puts non-empty values into $form_state['conf'].
 */
function emapping_map_pane_edit_form_submit($form, &$form_state) {
  foreach (element_children($form['map_settings']) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf']['map_settings'][$key] = $form_state['values'][$key];
    }
  }
}
