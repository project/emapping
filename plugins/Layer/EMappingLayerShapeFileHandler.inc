<?php

$plugin = array(
  'label' => t('Renders data source to ESRI shape file.'),
  'display_modules' => array('emapping_archive'),
  'source_classes' => array('eMappingSourceSQLHandler'),
  'handler' =>  array(
    'class' => 'EMappingLayerShapeFileHandler',
    'file' => 'EMappingLayerShapeFileHandler.class.php',
  ),
);

?>