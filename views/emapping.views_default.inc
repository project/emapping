<?php

/**
 * Implements hook_views_handlers().
 */
function emapping_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'emapping') . '/views',
    ),
  );
}

/**
 * Implements hook_views_data().
 *
 */
function emapping_views_data() {
  $data = array();
  return $data;
}

?>